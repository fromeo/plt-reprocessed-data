#To Do
#You should, really, spend some time to show a ratio plots between original and corrected luminosity (e.g. lumi_orbit_avg_w/lumi_orbit_avg). 
#This will be very helpful to spot inconsistency in the procedure (e.g. if lumi_orbit_avg_w/lumi_orbit_avg differ by ~>25%) that might arise for the most different reasons (e.g. bunch shifting as the TCDS issue in 2017)

#!/nfshome0/lumipro/brilconda/bin/python
import tables as t, pandas as pd, pylab as py, sys, numpy, math, os, Queue, csv
import struct
import os,sys
from matplotlib import pyplot as plt
from argparse import ArgumentParser

### --- Goal ---
#This script re-calculates bxraw, bx, avgraw, avg, in pltlumizero applying weights (eff, slope) on per channels values of luminosity read from pltaggzero
#In general, a time unit corresponds to an event. It is consistent with 
#pltaggzero: iterating 16 times, 1 for each channel of the plt
#pltlumizero: iterating 1 time (which takes into account (sum or average) the 16 channels of the plt)
#pltaggzero info
#sbil is mu (instantaneous luminosity per 4NB per bx (though it is a number withouth unit of measurement))
#3564 is the num of bunches in the LHC ring (the sum of their luminosity gives the integrated lumi per orbit)
#sbil_ch and sbil_avg are re-updated every time unit (which basically is over a orbit and iterating over the 16 channels)
#lumi_orbit is the instantaneous luminosity per orbit (the instant is the orbit time). It is the sum of sbil over all bunches in LHC
#NB
#It is possible that during the data taking some effects affect the stored data (raw['data']). Some examples are:
#1. Shifts in element positions 
#raw['data'] is corrected through rawDataFixed = numpy.roll(raw['data'],n)]), meaning its entries are shifted in position by n.
#n should normally be set to 0, unless a pathological behaviour is found during the reprocessing that points to a shift in the entries
#2. data are float at binary level, but are treated as int
#This happens in 2017 (until August), the data was being declared as int in one place but treated as a float in another place, so the results in the hd5 file are slightly garbled. 
#This requires their reinterpretation (a simply recast is not enough).
#binaries = [struct.pack('i', i) for i in raw['data']]
#rawDataFixed = [struct.unpack('f', b)[0] for b in binaries]

### --- Declare initial variables ---
#Input parameters
years = ['18']
parser = ArgumentParser()
parser.add_argument('-f', '--fill',     dest='fill',    action='store', type=int, default=6191) #The fill number to process
parser.add_argument('-i', '--ini',      dest='ini',     action='store', type=int, default=0) #Initial number of events to be parsed (must be multiple of 16)
parser.add_argument('-e', '--entries',  dest='entries', action='store', type=int, default=-1) #Final number of events to be parsed (-1 means all entries) (must be multiple of 16)
parser.add_argument('-p', '--plot',     dest='plot',    action='store', choices=[True,False], type=bool, default=False) #Either you want to see the plot or not
args = parser.parse_args()
fill = args.fill
ini = args.ini
entries = args.entries
plot = args.plot

#Declare initial quantities
nchannels = 16 #total number of channels in PLT (note that some channels might be disabled)
numBunchesLHC = 3564
sbil_ch = numpy.zeros((nchannels,numBunchesLHC)) 
sbil_avg = numpy.zeros(numBunchesLHC) #The average is upon the active channels and corresponds to the "bxraw" (or "bx") saved in pltlumizero (whic is mu or sbil) 
lumi_orbit_ch = [[] for i in range(nchannels)]
lumi_orbit_avg = [] #It corresponds to "avgraw" (or "avg") in pltlumizero
sbil_ch_w = numpy.zeros((nchannels,numBunchesLHC)) 
sbil_avg_w = numpy.zeros(numBunchesLHC) #The average is upon the active channels and corresponds to the 'sbil' saved in pltlumizero 
lumi_orbit_ch_w = [[] for i in range(nchannels)]
lumi_orbit_avg_w = [] 

##Weights and normalization values
#Note that the comparison of sbil_avg, lumi_orbit_avg with bxraw (bx), avgraw (avg) is valid given the same conditions of recording. This means:
#For 2018:
#disablech = [0,4]
#numdisabledch = 2
#For 2017:
#disabled channels and slopes are the same for the whole 2017
numdisabledch = 3
disablech = [0,3,4]
numeffectivech = nchannels-numdisabledch
slope_L = [-99, 0.0166,	0.0137, -99, -99, 0.0148, 0.0164, 0.0163, 0.0127, 0.0142, 0.0168, 0.0182, 0.0166, 0.0136, 0.0118, 0.0116]
slope_T = [-99, 0.0105, 0.0122, -99, -99, 0.0093, 0.0108, 0.0029, 0.0038, 0.0005, -0.0007, 0.0142, 0.0026, 0.0153, 0.0096, 0.0009]
#efficiencies vary per fill
fills_effs = {} #dictionary with fill as a key and an array of 16 values (1 each PLT channel, make sure the .csv files has columns with -99 for the missing channels)
shift=6 #shift in the column position that determines where PLT channel information starts
missing_fills_incsv = [5633, 5663, 5676, 5677, 5680, 5681, 5682, 5683, 5684, 5685, 5686, 5689, 5690, 5691, 5692, 5697] #for these fills, use the efficiency values calculated for the first fill
with open('2017PLTCorrections_Fills_5633_6371.csv') as csv_file:
 csv_reader = csv.reader(csv_file, delimiter=',')
 line_count = 0
 for row in csv_reader:
  #if 1 <= line_count and line_count < 5:
  if line_count>=1: #skip first file (line 0) has it contains the titles of the columns (check it on the .csv input file)
   effs = []
   for i in range(nchannels): effs.append(row[i+shift]) #Note that this assumes you have a column in the csv files for all 16 PLT channels, also those that are disable (have -99 in the column value)
   if line_count==1: #This is special for 2017, where David said that for the fills before the first fill in the csv file we can assign the weights of the first fills in the csv file
    for mf in missing_fills_incsv: fills_effs[int(mf)] = effs
   fills_effs[int(row[0])] = effs
  line_count += 1

##Normalization values
sigvis = 292.8
k = 11246./sigvis 

### --- Start the reprocessing ---
for year in years:
 print "The year is %s" % (year)
 print "The fill number is %s" % (fill)
 #Prepare input files and output dirs
 input_path = '/brildata/'+str(year)+'/'+str(fill)+'/'
 files = os.listdir(input_path)
 c = 0 #entries iterator, usefull if you want to test a small number of events
 stoparsing = False
 #files = ["7454_326894_1811191509_1811191517.hd5"] #Sometime it is faster to indicate a specific file, if you know some events of interest are there
 for file in files:
  if(stoparsing): break
  #file_="6165_302279_1709040733_1709040806.hd5" #"5883_297669_1706290554_1706290617.hd5"
  #if file!=file_ and file_!="": 
  # print "The file is %s continue" % (file)
  # continue
  print "The file is %s" % (file)
  h5in = t.open_file(input_path+file,mode='r')
  if("/beam" not in h5in or "/pltaggzero" not in h5in or "/pltlumizero" not in h5in): #Sometime the fill never reaches STABLE BEAMS and these groups are not written
   h5in.close()
   continue
  if(h5in.root.beam.nrows==0 or h5in.root.pltaggzero.nrows==0 or h5in.root.pltlumizero.nrows==0): #Sometime tables exists, but with zero entries
   h5in.close()
   continue
  table = h5in.root.pltaggzero
  channel_counter = 0 #bxmask = h5in.root.beam[channel_counter/nchannels]['collidable'] needs it and it has to start from 0 for every file
  bxmask = h5in.root.beam[0]['collidable'] #It will not change in the different entries, so no need to use h5in.root.beam[channel_counter/nchannels]['collidable']
  leading = (py.logical_xor(bxmask, py.roll(bxmask,1)) & bxmask)
  train = py.logical_xor(leading,bxmask)
  for row in table.iterrows(): #each row iterate over a channels. After 16 times, you go to a "new event"
   #Parse only n events
   if(c>=entries and entries!=-1):
    stoparsing = True
    break
   if((ini<=c and c<entries) or (entries==-1 and c>=ini)): 
    isdisabledch = 0 #0 means is not disabled 
    for i in disablech:
     if(i==int(row['channelid'])):
      isdisabledch = 1
      break
    if(isdisabledch==0):
     #Fix row['data'], if needed
     rawDataFixed = row['data'] #Initially no changes
     if year=='17':
      if fill<=6156: 
       binaries = [struct.pack('i', i) for i in rawDataFixed]
       rawDataFixed = [struct.unpack('f', b)[0] for b in binaries]
      if fill==5698:
       if(row['runnum']>=294947 and row['runnum']<294966): rawDataFixed = numpy.roll(rawDataFixed,828) 
      if fill==5839:
       if(row['runnum']==297050 and row['lsnum']>=8 and row['lsnum']<=69): rawDataFixed = numpy.roll(rawDataFixed,827)
      if fill==5883:
       if(row['runnum']<297670 or (row['runnum']==297670 and row['lsnum']<=31)): rawDataFixed = numpy.roll(rawDataFixed,2071)      
      if fill==6364:
       if((row['runnum']>306155 and row['runnum']<306169) or (row['runnum']==306155 and row['lsnum']>=1508) or (row['runnum']==306169 and row['lsnum']<=2)): rawDataFixed = numpy.roll(rawDataFixed,188)
     numorbits = math.ceil(float(max(rawDataFixed))/1024.)*1024.
     #Default measurements
     sbil_ch[int(row['channelid'])] = -1 * k * numpy.log([float(x)/numorbits for x in rawDataFixed])
     lumi_orbit_ch_ = sum(sbil_ch[int(row['channelid'])])
     lumi_orbit_ch[int(row['channelid'])].append(lumi_orbit_ch_)
     #Weighted measurements
     mu = -1 * k * numpy.log([float(x)/numorbits for x in rawDataFixed])
     eff = leading/float(fills_effs[fill][int(row['channelid'])])+train/float(fills_effs[fill][int(row['channelid'])])
     slope = leading*slope_L[int(row['channelid'])]/100+train*slope_T[int(row['channelid'])]/100
     mu = mu*eff
     mu = mu-mu*mu*slope
     sbil_ch_w[int(row['channelid'])] = mu
     lumi_orbit_ch_w_ = sum(sbil_ch_w[int(row['channelid'])])
     lumi_orbit_ch_w[int(row['channelid'])].append(lumi_orbit_ch_w_)
    if(int(row['channelid'])==nchannels-1):
     sbil_avg = (sbil_ch.sum(axis=0))/numeffectivech
     lumi_orbit_avg_ = sum(sbil_avg)
     lumi_orbit_avg.append(lumi_orbit_avg_) 
     sbil_avg_w = (sbil_ch_w.sum(axis=0))/numeffectivech
     lumi_orbit_avg_w_ = sum(sbil_avg_w)
     lumi_orbit_avg_w.append(lumi_orbit_avg_w_) 
     print 'run LS nb %s %s %s' % (row['runnum'],row['lsnum'],row['nbnum'])
     print "lumi_orbit_avg (Hz/ub): %s" % (lumi_orbit_avg_)
     print "lumi_orbit_avg_w (Hz/ub): %s" % (lumi_orbit_avg_w_)
     print ''
   channel_counter = channel_counter+1
   c = c+1
   #Print entries to keep track of the execution
   if (c%1600 == 0):
    print c
  h5in.close()

### --- Plot the results ---
if(plot):
 #colors = ["k.-","g.-","r.-","b.-","c.-","m.-","g^-","r^-","b^-","c^-","m^-","gs-","rs-","bs-","cs-","ms-"]
 colors = ["b","b","b","b","b","b","b","b","b","b","b","b","b","b","b","b"]
 py.style.use('seaborn-white')
 fig = py.figure(1)
 py.title("Fill %s, $\sqrt{s}$ = 13 TeV" % (fill), fontsize=60)
 ax = fig.add_subplot(111)
 #Ideal for shown plots
 py.text(0.05, 0.925,'CMS', ha='center', va='center', transform=ax.transAxes, fontsize=60, weight='bold') 
 py.text(0.145, 0.925,'Preliminary', ha='center', va='center', transform=ax.transAxes, fontsize=60, style='italic')
 py.text(0.24, 0.925,'2017', ha='center', va='center', transform=ax.transAxes, fontsize=60) #, weight='bold') 
 #Ideal for saved plots
 #py.text(0.05, 0.925,'CMS', ha='center', va='center', transform=ax.transAxes, fontsize=60, weight='bold') 
 #py.text(0.180, 0.925,'Preliminary', ha='center', va='center', transform=ax.transAxes, fontsize=60, style='italic')
 #py.text(0.31, 0.925,'2017', ha='center', va='center', transform=ax.transAxes, fontsize=60) #, weight='bold') 
 lumi_orbit_ActiveCh = [loac for loac in lumi_orbit_ch if loac]
 y_max = 1.5*max((max(m) for m in lumi_orbit_ActiveCh))
 py.ylim((0,y_max))
 isNotFirst=False
 for i in range(0,nchannels):
  if(i not in disablech):
   if isNotFirst: py.plot(range(int(len(lumi_orbit_ch[i]))),lumi_orbit_ch[i],colors[i],label="")
   else: py.plot(range(int(len(lumi_orbit_ch[i]))),lumi_orbit_ch[i],colors[i],label="PLT channels")
   isNotFirst=True
   py.legend(loc='upper right', frameon=False, prop={'size': 70})
   py.xlabel('Time (sec)',size=60)#, horizontalalignment='right', x=1.0)
   py.ylabel('PLT luminosity (Hz/$\mu$b)',size=60)#, verticalalignment='top')
   py.tick_params(axis='x', labelsize=40)
   py.tick_params(axis='y', labelsize=40)
   mng = py.get_current_fig_manager()
   mng.window.showMaximized() # QT backend
   #mng.resize(*mng.window.maxsize()) # TkAgg backend
   #mng.frame.Maximize(True) # WX backend
 name = "Lumi_"+str(fill)+"_"+str(ini)+"_"+str(entries)+".png"
 fig.set_size_inches((32, 18))#Number are W and H respectively, here adjusted to fit screen size
 fig.savefig(name, bbox_inches='tight')
 
 fig = py.figure(2)
 py.title("Fill %s, $\sqrt{s}$ = 13 TeV" % (fill), fontsize=60)
 ax = fig.add_subplot(111)
 #Ideal for shown plots
 py.text(0.05, 0.925,'CMS', ha='center', va='center', transform=ax.transAxes, fontsize=60, weight='bold') 
 py.text(0.145, 0.925,'Preliminary', ha='center', va='center', transform=ax.transAxes, fontsize=60, style='italic')
 py.text(0.24, 0.925,'2017', ha='center', va='center', transform=ax.transAxes, fontsize=60) #, weight='bold') 
 #Ideal for saved plots
 #py.text(0.05, 0.925,'CMS', ha='center', va='center', transform=ax.transAxes, fontsize=60, weight='bold') 
 #py.text(0.180, 0.925,'Preliminary', ha='center', va='center', transform=ax.transAxes, fontsize=60, style='italic')
 #py.text(0.31, 0.925,'2017', ha='center', va='center', transform=ax.transAxes, fontsize=60) #, weight='bold') 
 py.ylim((0,y_max))
 isNotFirst=False
 for i in range(0,nchannels):
  if(i not in disablech):
   if isNotFirst: py.plot(range(int(len(lumi_orbit_ch_w[i]))),lumi_orbit_ch_w[i],colors[i],label="")
   else: py.plot(range(int(len(lumi_orbit_ch_w[i]))),lumi_orbit_ch_w[i],colors[i],label="PLT channels")
   isNotFirst=True
   py.legend(loc='upper right', frameon=False, prop={'size': 70})
   py.xlabel('Time (sec)',size=60)#, horizontalalignment='right', x=1.0)
   py.ylabel('Reweighted PLT luminosity (Hz/$\mu$b)',size=60)#, verticalalignment='top')
   py.tick_params(axis='x', labelsize=40)
   py.tick_params(axis='y', labelsize=40)
   mng = py.get_current_fig_manager()
   mng.window.showMaximized() # QT backend
   #mng.resize(*mng.window.maxsize()) # TkAgg backend
   #mng.frame.Maximize(True) # WX backend
 name = "LumiWeighted_"+str(fill)+"_"+str(ini)+"_"+str(entries)+".png"
 fig.set_size_inches((32, 18))#Number are W and H respectively, here adjusted to fit screen size
 fig.savefig(name, bbox_inches='tight')
 
 py.show()
